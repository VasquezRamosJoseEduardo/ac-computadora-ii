# Computadora II
```plantuml
@startmindmap
*[#lightblue] 1.- Computadoras Electrónicas
	*[#lightblue] Razon de su creación
		*[#lightblue] Lo ordenadores anteriores tenían\nun muy costoso mantenimiento y\nocupaban mucho lugar, por lo que\nera necesario una innovación.
	*[#lightblue] Harvard Mark I
		*[#lightblue] Caracteristicas
			*[#lightblue] Tenía 750000 componentes
			*[#lightblue] 80KM de cable
			*[#lightblue] Eje de 15m
			*[#lightblue] Motor de 15 caballos de fuerza
		*[#lightblue] Usos
			*[#lightblue] Dentro de todos sus usos el\nmas significativo fue su\nutilización en el desarrollo\n de la bomba nuclear
		*[#lightblue] Grace Hopper
			*[#lightblue] Una vez sacaron una polilla\nde uno de los relés de esta\ndebido a esto Grace Hooper\ndijo: "Desde entonces, cuando algo salia mal\ncon una computadora deciamos que teniamos\nbichos(bugs)". Eh aquí el origen\nde este termino.
	*[#lightblue] Relé
		*[#lightblue] ¿Qué era?
			*[#lightblue] Interruptor mecanico controlado electricamente
		*[#lightblue] Compuesto por:
			*[#lightblue] Bobina
			*[#lightblue] Brazo de hierro
			*[#lightblue] 2 Contactos
		*[#lightblue] Funcionamiento
			*[#lightblue] Cuando una corriente electrica atraviesa la bobina\nse genera un campo magnetico, este\natrae al brazo mecanico y este hace que se\nconecten los dos contactos cerrando el\ncircuito.
		*[#lightblue] Problematica
			*[#lightblue] El brazo de hierro, es que le\nbrazo mecanico tenía inercia.\nLo cual generaba un problema de velicidad.
			*[#lightblue] Su vida util, por el daño que\nse podian llevar sus partes\n debido al uso.
			*[#lightblue] Entre mas relés mas podía fallar,\nla MARK I tenía 3500, fallaba al menos\nun relé al día.
			*[#lightblue] Atraía insectos
		*[#lightblue] Capacidades
			*[#lightblue] 3 sumas o restas por segundo
			*[#lightblue] Multiplicación en 6 segundos
			*[#lightblue] División en 15 segundos
			*[#lightblue] Operaciones complejas llevanan\nminutos. Ejemplo: Funciones Trigonometricas. 
	*[#lightblue] Válvula Termoiónica
		*[#lightblue] Creada por:
			*[#lightblue] Jonh Ambrose Fleming en 1904
		*[#lightblue] Componentes
			*[#lightblue] Filamento
			*[#lightblue] Ánodo 
			*[#lightblue] Cátoddo
			*[#lightblue] Bulbo de cristal
		*[#lightblue] Funcionamiento
			*[#lightblue] Cuando el filamento se calienta\npermite el flujo de electrones desde\nel ánodo hasta el cátoddo, esto permite el\nflujo de la corriente en una sola dirección.
		*[#lightblue] Lee De Forest
			*[#lightblue] En 1906 agregó un tercer electrodo,\nal cual se le denominó electrodo de control
			*[#lightblue] Electrodo de control (Tubo de vacio)
				*[#lightblue] Este se situaba entre en ánodo\ny el cátoddo del diseño de Fleming
				*[#lightblue] Al aplicar una carga positiva\no negativa, permite o detiene la corriente\nrespectivamente
				*[#lightblue] Lograba lo mismo que los relés\npero sin el problema de las partes mobiles,\npor lo tanto  hacía que tuviera mas confiabilidad\y mayor velicidad.
				*[#lightblue] Fueron la base para las radios\nlos telefonos de larga distacia, etc.
				*[#lightblue] Fallos
					*[#lightblue] Eran muy fragiles
					*[#lightblue] Se podian quemar facilmente
					*[#lightblue] Costosos
				*[#lightblue] En 1940 se hizo mas accesible su uso
				*[#lightblue] Usos en la\ncomputación
					*[#lightblue] Colossus Mark I
	*[#lightblue] Colossus Mark I
		*[#lightblue] Diseñada por el ingeniero Tommy Flowers\nen diciembre de 1943
		*[#lightblue] Fue instalada en Bletchley - Reino Unido
			*[#lightblue] Ayudaba a decodificar las comunicaciones nazis
				*[#lightblue] TheBombe
					*[#lightblue] Creado en 1941 por Alan Turing
					*[#lightblue] Fue hecho para decifrar el código\nenigma nazi
					*[#lightblue] Pero esta no fue una computadora
		*[#lightblue] El primer diseño de la Colossus I
			*[#lightblue] Tenía 1600 tubos de vacio
			*[#lightblue] En total se crearon 10 Colossus
			*[#lightblue] Pero esta fue considerada la primer\ncomputadora electronica programable
			*[#lightblue] Programación
				*[#lightblue] Se realizaba mediante la conexión\nde cientos de cables, eran parecidos a\nlos antiguos tableros de conmutación de los\ntelefonos
		*[#lightblue] ENIAC
			*[#lightblue] Creada en 1946 en la universidad de Pensilvania\nporJohn Mauchly y J.Presper Eckert
			*[#lightblue] Siglas
				*[#lightblue] Calculadora
				*[#lightblue] Integradora
				*[#lightblue] Numérica
				*[#lightblue] Electrónica
			*[#lightblue] Se le denominó la primera Computadora\nelectrónica programable de proposito\ngeneral
			*[#lightblue] Capacidad
				*[#lightblue] Podía realizas 5000 sumas y restas\nde 10 digitos por segundo.
				*[#lightblue] Se decía que hizo mas calculos que\ntoda la humanidad hasta este momento.
			*[#lightblue] Fallo
				*[#lightblue] A medio día se dañaba debido\na los tubos de vacío
		*[#lightblue] En 1950 se estaba termindando la epoca de los tubos de vacío.
	*[#lightblue] Transistor
		*[#lightblue] En 1947 en Bell Laboratories los cientificos\nJonh Bardeen, Walter Brattain y William\nShockley lo crearon y marcó una nueva era\nen la computación.
		*[#lightblue] ¿Qué es?
			*[#lightblue] Tenían la misma función que los\nrelés y los tubos de vacío, es decir\nun interruptor electrico
		*[#lightblue] Caracteristicas
			*[#lightblue] Estaban hechos de silicio, ya\nque en su estado puro es aislante
			*[#lightblue] Doping 
				*[#lightblue] Se le agregaba 2 elementos mas que,\nhacían posible el paso de corriente, el cual podia tener exeso de electrones
		*[#lightblue] Constituido por
			*[#lightblue] Colector
			*[#lightblue] Emisor
			*[#lightblue] Base
		*[#lightblue] Funcionamiento
			*[#lightblue] Cuando hay un flujo de corriente entre\nla base y el emisor este permite que haya flujo\nde corriente entre el emisor y el colecotor\nasí cumplia la función de un interruptor
			*[#lightblue] A diferencia de los relés y de los\ntubos de vacío eran mucho mas\nresistente y mas pequeños
		*[#lightblue] La primera computadora a base de transistores\npodía realizar
			*[#lightblue] 4500 sumas
			*[#lightblue] 80 divisiones o multiplicaciones
			*[#lightblue] IBM implemtó este modelo en todos sus\nmodelos
		*[#lightblue] Hoy en día las computadoras basadas\nen transistores tienen un tamaño menor a 50nm 
			*[#lightblue] Diminutos
			*[#lightblue] Rápidos
@endmindmap


```
# Arquitectura Von Neumann y Arquitectura Harvard
```plantuml
@startmindmap
*[#Orange] 2.- Arquitectura Von Neumann y Arquitectura Harvard
	*[#Orange] Ley de Moore
		*[#Orange] Establece
			*[#Orange] La velocidad del procesador o el poder\nde procesamiento total de las computadoras\nse duplica cada doce meses
		*[#Orange] Electronica
			*[#Orange] El numero de transistores por chip\npermanece sin cambios
			*[#Orange] El costo del chip permanece sin\ncambios
		*[#Orange] Performance
			*[#Orange] Se incrementa la velocidad del procesador
			*[#Orange] Se incrementa la velocidad de la memoria
			*[#Orange] La velocidad de la memoria corres siempre\npor detras de la del procesador
		*[#Orange] Sistemas cableados
			*[#Orange] Interprete de instrucciones
			*[#Orange] Señales de control
			*[#Orange] Datos
			*[#Orange] Secuencia de funciones aritmeticas\nlógicas
			*[#Orange] Resultados
	*[#Orange] Arquitectura Von Neumann
		*[#Orange] Con este modelo surge el concepto de programa\nalmacenado, por el cual se les conoce a las\ncomputadoras de este tipo también
		*[#Orange] Es una arquitectura fue descrita en 1945\npor John Von Neumann
		*[#Orange] Modelo
			*[#Orange] Los datos y programas se almacenan\nen una misma memoria de lectura-escritura
			*[#Orange] Ejecucion en secuencia
			*[#Orange] Representacion binaria
		*[#Orange] Contiene
			*[#Orange] CPU
				*[#Orange] Contiene la unidad de control, una unidad\naritmetica logica y registros
			*[#Orange] Memoria principal
				*[#Orange] Puede almacenar tanto instrucciones como datos
			*[#Orange] Sistema de entrada/salida
		*[#Orange] Cuello de botella de Neumann
			*[#Orange] La separación de la memoria y la CPU acarreó un problema denominado Neumann bottleneck
			*[#Orange] Se debe a que la cantidad de datos que pasa entre\nestos dos elemnetos difiere mucho en tiempo con las\nvelocidades de ellos, por lo cual la CPU puede permanecer\nociosa
		*[#Orange] Un disco SSD y un disco HDD no funcionan a la misma velocidad
			*[#Orange] Cada elemento de la computadora\ntrabajan a diferentes velocidades
		*[#Orange] Acceso mecanico
			*[#Orange] Con un disco ssd es mas rapido. ya que\nlo hace de manera dinamica
	*[#Orange] Modelo de Bus
		*[#Orange] El bus es un dispositivo en comun entre dos o mas dispositivos
		*[#Orange] Cada linea puede transmitir señales que representan\nunos y ceros, en secuencia, de a una señal por unidad\nde tiempo.
		*[#Orange] El modelo de Bus es un refinamiento del modelo de Von Neumann
			*[#Orange] Su proposito es el de reducir la cantidad de conexiones entre la CPU y sus sistemas
		*[#Orange] Todos los componentes se comunican\na tráves de un sistema de buses\nque se dividen en
			*[#Orange] Bus de datos
			*[#Orange] Bus de direcciones
			*[#Orange] Bus de control
	*[#Orange] Instrucciones
		*[#Orange] La funcion de una computadora es la ejecución de programas.
			*[#Orange] Los programas se encuentran localizados en memoria\ny consisten de instrucciones
		*[#Orange] La CPU es quien se ecarga de ejecutar dichas\ninstrucciones atraves de un ciclo denominado ciclo de instruccion
			*[#Orange] Las instrucciones consisten de secuencias de 1 y 0\nllamados codigos maquina
			*[#Orange] Por ellos se emplean lenguajes como el ensamblador u otros lenguajes
				*[#Orange] Python
				*[#Orange] Java
				*[#Orange] Las instrucciones son ejecutadas por la CPU a\ngrandes velocidades
					*[#Orange] 3millones de operaciones por segundo para unba CPU que opera a 3GHz 
	*[#Orange] Ciclo de ejecución
		*[#Orange] Paso 1: La unidad de control obtiene la proxima instruccion\nde memoria (usando el registro PC) y dejando la información\nen el registro IR
		*[#Orange] Paso 2: Se incrementa el PC
		*[#Orange] Paso 3: Las instrucciones son decodificadas a un lenguaje que entiende la ALU
		*[#Orange] Paso 4: Obtiene de memoria los operando requeridos\npor la instruccion
		*[#Orange] Paso 5: La ALU ejecuta y deja los resultados\nen registros o en memoria
		*[#Orange] Paso 6: Volver al paso 1
	*[#Orange] Arquitectura de Harvard
		*[#Orange] Modelo
			*[#Orange] Utilizaban dispositivos de almacenamiento fisicamente\nseparados para las instrucciones y para los\ndatos
		*[#Orange] Contenido
			*[#Orange] Describe una arquitectura de diseño para\nun computador digital electronico con partes\nque constan de
				*[#Orange] Unidad Central de Procesamiento
					*[#Orange] Contiene una unidad de control, una unidad\naritmetica lógica y registros
				*[#Orange] Memorial Principal
					*[#Orange] Puede almacenar instrucciones y datos
				*[#Orange] Sistema de entrada/salida
		*[#Orange] Memorias
			*[#Orange] Fabricar memorias mucho mas rapidas tiene un precio muy alto
				*[#Orange] La solucion por tanto, es proporcionar una\npequeña cantidad de memoria muy rapida conocida\ncon el nombre de CACHE
					*[#Orange] Mientras los datos que necesita el\nprocesador entén en la caché, el rendimiento\nserá mucho mayor
			*[#Orange] Solución Harvard
				*[#Orange] Las instrucciones y los datos se\nalmacenan en cachés separadas para mejorar el rendimiento
					*[#Orange] Se unan en
						*[#Orange] PICs
						*[#Orange] Microcontroladores
		*[#Orange] Arquitectura 
			*[#Orange] Procesador
				*[#Orange] Dos unidades funcionales principales
					*[#Orange] Unidad de control(UC)
					*[#Orange] Unidad aritmetica logica(ALU)
			*[#Orange] Memoria de instrucciones
				*[#Orange] Es la memoria donde se almacenan las\ninstrucciones del programa que debe\nejecutar el microcontrolador
					*[#Orange] ROM
					*[#Orange] PROM
					*[#Orange] EEPROM
					*[#Orange] flash
			*[#Orange] Memoria de datos
				*[#Orange] Se almacenan los datos utilizados por los\nprogramas
			*[#Orange] Se utilizan en productos o sistemas de proposito\nespecificos
				*[#Orange] Electrodomesticos
				*[#Orange] Telecomunicaciones
				*[#Orange] Autos
				*[#Orange] Mause
				*[#Orange] Impresoras
				*[#Orange] Robotica
@endmindmap


```
# Basura Electrónica
```plantuml
@startmindmap
*[#lightgreen] 3.- Basura Electrónica
	*[#lightgreen] ¿Chatarra o fuente de dinero?
		*[#lightgreen] Se extrae oro de los productos encontrados,\nya que algunos tienen metales precioso
			*[#lightgreen] Oro
			*[#lightgreen] Cbalto
			*[#lightgreen] Cobre
		*[#lightgreen] Datos
			*[#lightgreen] Las personas tiran sus aparatos con mucha\ninformación en ellos, esta tambien es valisa
			*[#lightgreen] Algunos de estos datos se utilizan de\nmanera ilegal.
		*[#lightgreen] Reciclaje Inseguro
			*[#lightgreen] Una pila puede contaminar 600 litros\nde agua
			*[#lightgreen] Se exporta a paises de tercer mundo\ndonde sus aplicaciones repercuten negativamente\nen el ambiente
				*[#lightgreen] China
				*[#lightgreen] India
				*[#lightgreen] México
			*[#lightgreen] Se dice que 1 de cada 3 contenedores\nexportados por la union europea, contiene\nbasura electronica ilegal
				*[#lightgreen] Se asegura que esta será una gran plaga en el siglo 21
			*[#lightgreen] Los gobiernos ponen medidas para\ncontrolar los residuos electronicos
		*[#lightgreen] Porqué se tira
			*[#lightgreen] Se tiran cuando terminan su vida util
			*[#lightgreen] El reciclaje de estos materiales\ntiene un proceso costoso y complicado
				*[#lightgreen] En paise como india se utilizan\nprocesos antiguos y baratos
				*[#lightgreen] Pero al hacerlo respiran los quimicos\ntoxicos y los materiales terminan en rios\nlo cual afecta al medio ambiente
				*[#lightgreen] Los residuos quemados contaminan\nel agua y el aire.
				*[#lightgreen] Los trabajadores involucrados\nsufren grandes problemas de salud debido a el trabajo\nque realizan, trabajadores que deberian de vivir\hasta los 70, mueren a los 45 
	*[#lightgreen] e-end
		*[#lightgreen] Es un centro de Reciclaje
		*[#lightgreen] Montones de basura electronica se procesan diariamente
		*[#lightgreen] Datos
			*[#lightgreen] Tienen un iman gigante que elimina todos\nlos datos de los discos duros
			*[#lightgreen] O grandes trituradoras que se encargan\nde destruir los discos duros.
	*[#lightgreen] ¿Cómo se tratan los residuos?
		*[#lightgreen] Se puede llegar a reciclar hasta el 95% de la basura electronica
		*[#lightgreen] Las grandes corporaciones se encargan\nde que la vida util de los aparatos sea\corta, esto hace que incremente la nececidad\nde comprar mas y mas, debido a esto hay mas\ndesechos
		*[#lightgreen] Peru
			*[#lightgreen] Se estima que hay 400000 toneladas de basura electronica
			*[#lightgreen] Por cada 1 tonelada de aluminio\nse emiten 3.8 toneladas de Co2
		*[#lightgreen] RAE
			*[#lightgreen] De esta manera se le conoce a la basura electronica
			*[#lightgreen] Residuos de aparatos electricos y electronicos.
			*[#lightgreen] Ejemplos:
				*[#lightgreen] Celular
				*[#lightgreen] Licuadora
				*[#lightgreen] Computadora
				*[#lightgreen] Television
		*[#lightgreen] Reciclaje (Transformar basura electronica en materia prima)
			*[#lightgreen] La mayoria de gente tira los residuos\na la basura directamente
			*[#lightgreen] Se tienen que clasificar
				*[#lightgreen] Se les clasifica por su contenido\nde minerales
					*[#lightgreen] Altos grado
					*[#lightgreen] Bajos grado
			*[#lightgreen] Proceso
				*[#lightgreen] Se desarman para conservar los materiales\nprimarios, para poder usarlos en la elaboracion\nde nuevos productos
					*[#lightgreen] Metales
					*[#lightgreen] Plastico
				*[#lightgreen] Se pesa para poder dar una recompensa por estos mismos
			*[#lightgreen] Los materiales que contaminan se llevan a\nun"relleno de seguridad" que esta hecho para poder\ncontener los derrames de estos productos.
			*[#lightgreen] Los materiales reciclables tienen tratamientos\ndiferentes a los materiales peligrosos
			*[#lightgreen] El 70% de las toxinas que se desprenden\nde los tiraderos de basura, provienen de\nbasura electronica
			*[#lightgreen] Algunos de los desechos son funcionales
			*[#lightgreen] De estos productos se pueden sacar otros\nestos productos se producen en otras empresas
				*[#lightgreen] Zapatos
				*[#lightgreen] Partes de automoviles
				*[#lightgreen] Adornos de feretos
				*[#lightgreen] Adoquines
				*[#lightgreen] Reusar monitores
				*[#lightgreen] Cargadores de Laptops
	*[#lightgreen] Laptos funcionales
		*[#lightgreen] En empresas donde se recicla basura\nelectronica se almacenan laptos, pero\nno solo eso, si no tambien
			*[#lightgreen] Ratones
			*[#lightgreen] Cables
			*[#lightgreen] Teclados
			*[#lightgreen] Incluso mochilas
		*[#lightgreen] En algunos casos se tiene que pagar para\poder tirar algunos de estos desechos
		*[#lightgreen] Productos que aunque funcionales, se deben\nde destruir debido a la información de las empresas
		*[#lightgreen] Cementerios de computadoras
			*[#lightgreen] Algunos equipos tienen partes\nfuncionales las cuales se pueden vender por separado
			*[#lightgreen] Se tiran demasiado equipos funcionales
			*[#lightgreen] Tambien se tiran cosas valisosas como:
				*[#lightgreen] Planchas
				*[#lightgreen] Consolas de videojuegos
				*[#lightgreen] Radios
				*[#lightgreen] Computadoras que en su entonces valian mas\nde 5000usd
				*[#lightgreen] Rebobinadoras
	*[#lightgreen] Tesoros Electronicos
		*[#lightgreen] Se pueden llegar a encontrar articulos\ncoleccionables		
			*[#lightgreen] Videojuegos
			*[#lightgreen] Caseteras
			*[#lightgreen] Telefonos antiguos
			*[#lightgreen] Camaras
			*[#lightgreen] Walkmans
			*[#lightgreen] Camaras cinematograficas
			*[#lightgreen] Tablets
			*[#lightgreen] Teclados
@endmindmap